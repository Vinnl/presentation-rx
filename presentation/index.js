// Import React
import React from "react";
import CodeSlide from 'spectacle-code-slide';

// Import Spectacle Core tags
import {
  Appear,
  BlockQuote,
  Cite,
  CodePane,
  Deck,
  Fill,
  Heading,
  Image,
  Layout,
  Link,
  ListItem,
  List,
  Markdown,
  Quote,
  Slide,
  Spectacle,
  Text
} from "spectacle";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Import custom component
import Interactive from "../assets/interactive";

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");


const images = {
  city: require("../assets/city.jpg"),
  demonstration: require("../assets/demonstration.jpg"),
  kat: require("../assets/kat.png"),
  logo: require("../assets/formidable-logo.svg"),
  react: require("../assets/react.png"),
  cyclejs: require("../assets/cyclejs.png"),
  cycle_lifecycle: require("../assets/cycle_lifecycle.png"),
  cycle_lifecycle_app: require("../assets/cycle_lifecycle_app.png"),
  cycle_lifecycle_input: require("../assets/cycle_lifecycle_input.png"),
  cycle_lifecycle_render: require("../assets/cycle_lifecycle_render.png"),
  stream: require("../assets/stream.png"),
  map: require("../assets/map.png"),
  scan: require("../assets/scan.png"),
  merge: require("../assets/merge.png"),
  mutation: require("../assets/mutation.png"),
  markdown: require("../assets/markdown.png")
};

preloader(images);

const theme = createTheme({
  primary: "#ff4081"
});

export default class Presentation extends React.Component {
  render() {
    return (
      <Spectacle theme={theme}>
        <Deck transition={["zoom", "slide"]} transitionDuration={500}>
          <Slide transition={["zoom"]} bgColor="primary">
            <Heading size={2} fit caps lneHeight={1} textColor="white">
              Down the FRP rabbit hole
            </Heading>
            <Heading size={1} fit caps textColor="black">
              Observables all the way down
            </Heading>
            <Text>By Vincent Tunru</Text>
          </Slide>
          <Slide transition={["slide"]} bgColor="tertiary">
            <Heading caps fit size={1} textColor="primary">
              Today's adventure
            </Heading>
            <Markdown>
              {`
* Bugs, bugs, bugs
* Observables and FRP
* Demo
* Going all-in
* Conclusions
              `}
            </Markdown>
          </Slide>
          <Slide transition={["slide"]} bgColor="black">
            <BlockQuote>
              <Quote>If debugging is the process of removing bugs, then programming must be the process of putting them in.</Quote>
              <Cite>Edsger W. Dijkstra</Cite>
            </BlockQuote>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <CodePane
              lang="js"
              textSize={30}
              source={require("raw!../assets/state.example")}
              margin="20px auto"
            />
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <CodePane
              lang="js"
              textSize={30}
              source={require("raw!../assets/stateless.example")}
              margin="20px auto"
            />
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <CodePane
              lang="js"
              textSize={30}
              source={require("raw!../assets/pure_state.example")}
              margin="20px auto"
            />
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <CodePane
              lang="js"
              textSize={30}
              source={require("raw!../assets/impure_state.example")}
              margin="20px auto"
            />
          </Slide>
          <Slide transition={["slide"]} bgImage={images.city.replace("/", "")} bgDarken={0.75}>
            <Heading size={1} caps fit textColor="primary">
              Functional Reactive Programming
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Heading>
              The Observable
            </Heading>
            <CodePane
              lang="js"
              textSize={30}
              source={
`anObservable$
.subscribe(doSomething);
` }
              margin="20px auto"
            />
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Image src={images.stream.replace("/", "")} margin="0px auto 40px"/>
            <Markdown>{`(Courtesy of [RxMarbles.com](http://rxmarbles.com/))`}</Markdown>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Image src={images.map.replace("/", "")} margin="0px auto 40px"/>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Image src={images.scan.replace("/", "")} margin="0px auto 40px"/>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Image src={images.merge.replace("/", "")} margin="0px auto 40px"/>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Heading>
              Demo
            </Heading>
            <CodePane
              lang="html"
              textSize={20}
              source={require("raw!../assets/html.example")}
              margin="20px auto"
            />
          </Slide>
          <CodeSlide
            transition={[]}
            lang="js"
            code={require("raw!../assets/index.example")}
            ranges={[
              { loc: [0, 0] },
              { loc: [4, 6] },
              { loc: [3, 8] },
              { loc: [10, 15] },
              { loc: [2, 16] },
              { loc: [25, 30] },
              { loc: [19, 20] },
              { loc: [19, 21] },
              { loc: [18, 22] },
              { loc: [18, 23] },
              { loc: [18, 24] },
              { loc: [17, 24] }
            ]}
          />
          <Slide transition={["slide"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Heading>Cycle.js</Heading>
            <Image src={images.cyclejs.replace("/", "")} margin="0px auto 40px"/>
          </Slide>
          <Slide transition={["fade"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Image src={images.cycle_lifecycle_app.replace("/", "")} margin="0px auto 40px"/>
          </Slide>
          <Slide transition={["fade"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Image src={images.cycle_lifecycle_render.replace("/", "")} margin="0px auto 40px"/>
          </Slide>
          <Slide transition={["fade"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Image src={images.cycle_lifecycle_input.replace("/", "")} margin="0px auto 40px"/>
          </Slide>
          <Slide transition={["fade"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Image src={images.cycle_lifecycle.replace("/", "")} margin="0px auto 40px"/>
          </Slide>
          <Slide transition={["slide"]}>
            <Markdown>
              {`
* Code that is hard to follow has more bugs
* Side effects make code hard to follow
* Pure functions eschew side effects
* FRP makes programming with pure functions practical

More: [VincentTunru.com](https://vincenttunru.com/)
              `}
            </Markdown>
          </Slide>
          <Slide transition={["fade"]} bgColor="primary" notes="<ul><li>talk about that</li><li>and that</li></ul>">
            <Image src={images.react.replace("/", "")} margin="0px auto 40px"/>
          </Slide>
        </Deck>
      </Spectacle>
    );
  }
}
